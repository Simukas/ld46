extends Node2D

export (float) var mouseStillTime
var mouseStillTimer = 0
var mouseStill = false
var detailingObject = false

func _ready():
	Events.connect('RequestDetailView', self, 'processDetailViewRequest')

func _unhandled_input(event):

	if event is InputEventMouseMotion:
		if mouseStill:
			mouseMovementDetected()
		mouseStillTimer = 0

func mouseMovementDetected():
	print('mouse no longer still')
	mouseStill = false
	Events.emit_signal('MouseNoLongerStill')
	hide()

func mouseStillnessDetected():
	print('mouse still')
	mouseStill = true
	Events.emit_signal('MouseStill')	

func _process(delta):

	if not mouseStill:
		mouseStillTimer += delta

		if mouseStillTimer >= mouseStillTime:
			mouseStillnessDetected()

func hide():
	if detailingObject:
		detailingObject.detailView = false
	detailingObject = false
	visible = false

func processDetailViewRequest(newPos, textToShow, object):
	print('request for detail view Received')
	position = newPos
	$Label.text = textToShow

	if detailingObject:
		detailingObject.detailView = false

	detailingObject = object
	detailingObject.detailView = self

	visible = true

func updateText(textToShow):
	$Label.text = textToShow
