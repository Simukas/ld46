extends Node

signal GameOver ()
signal TileScored()

signal TileRemoved()
signal FreeTileTaken()
signal Stability()

signal TileRefresh()
signal TileDragStarted()
signal TileDecayed()
signal TilePlaced()

signal BonusScore()

signal MouseStill()
signal MouseNoLongerStill()

signal RequestDetailView(pos, text, object)