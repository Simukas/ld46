extends Node2D

export var movable = true
export (Texture) var greenArrow
export (Texture) var redArrow
var dragged = false
var dragOffset = Vector2(0,0)
var highlightedSlot = false
var intersectingSlots = []
var slot = false
var detailView = false
var tileName = 'defaultTileName'
var mouse_over = false
onready var decay = get_node("AnimRoot/Decay")
onready var stateEffects = get_node("AnimRoot/StateEffects")
onready var emotionSprite = get_node('AnimRoot/EmotionSprite')

var influences = [1,-1,1,-1,0,1]
var multiplier = 1

class_name Tile

func _ready():
	updateInfluenceSprites()
	var _c = decay.connect('Decayed',self, 'dieToDecay')
	decay.decayingInstance = self

	decay.connect('Healed', stateEffects, 'heal')
	decay.connect('Damaged', stateEffects, 'decay')
	decay.connect('NoChange', stateEffects, 'neutral')

	Events.connect('TilePlaced', self, 'hideInactiveDirections')
	Events.connect('TileRemoved', self, 'hideInactiveDirections')
	Events.connect('MouseStill', self, 'checkForDetailView')

func _process(_delta):
	
	if dragged:
		position = get_viewport().get_mouse_position() - dragOffset

func _on_Shape_gui_input(event):

	if event is InputEventMouseButton and event.is_pressed():
		print('starting drag')
		if movable:
			dragOffset = get_local_mouse_position()
			print(get_local_mouse_position())
			dragged = true
			Events.emit_signal('TileDragStarted')
			resetInfluenceSpriteVisibility()

	elif event is InputEventMouseButton and not event.pressed:
		if dragged:
			dragged = false
			
			var isDropped = dropIntoSlot()
			if isDropped:
				recordSlot(intersectingSlots.back())
				Events.emit_signal('TilePlaced')
			elif slot:
				position = slot.position

			#if slot:
			#	hideInactiveDirections()
				
			intersectingSlots = []
			updateHighlighted()

func recordSlot(newSlot):
	if slot:
		slot.removeTile()
	slot = newSlot
	decay.active = slot.tileDecayActive

func _on_Area2D_area_entered(area):

	if area.get_parent() is HexSlot and not area.get_parent().tileContained:
		intersectingSlots.append(area.get_parent())
		updateHighlighted()

func _on_Area2D_area_exited(area):

	if area.get_parent() is HexSlot:
		var index = intersectingSlots.find(area.get_parent())
		if index >= 0:
			intersectingSlots.remove((index))
			updateHighlighted()

func updateHighlighted():

	var newHighlighted = intersectingSlots.back()
	if not newHighlighted:
		if highlightedSlot:
			highlightedSlot.dehighlight()
			highlightedSlot = false
		return

	if not highlightedSlot:
		highlightedSlot = newHighlighted
		highlightedSlot.highlight()
		return
	
	if newHighlighted != highlightedSlot:
		highlightedSlot.dehighlight()
		highlightedSlot = newHighlighted
		highlightedSlot.highlight()

func dropIntoSlot():
	var slotToDrop = intersectingSlots.back()
	if slotToDrop:
		return slotToDrop.addTile(self)

	return false

func updateInfluenceSprites():
	var cnt = 0
	var influenceSprites = $AnimRoot/Influences.get_children()
	for influence in influences:
		var influenceSprite = influenceSprites[cnt]
		if influence > 0:
			influenceSprite.texture = greenArrow
		elif influence < 0:
			influenceSprite.texture = redArrow
		elif influence == 0:
			influenceSprite.visible = false
		cnt+=1

func hideInactiveDirections():

	var inactiveDirections = slot.getAvailableDirectionArray()
	var cnt = 0
	var influenceSprites = $AnimRoot/Influences.get_children()

	for situation in inactiveDirections:
		var influenceSprite = influenceSprites[cnt]
		match situation:
			'tile':				
				influenceSprite.self_modulate = ColorN('white')
			'empty':				
				influenceSprite.self_modulate = Color8(255,255,255,50)
			'wall':
				influenceSprite.self_modulate = Color8(255,255,255,0)
		cnt+=1

	if detailView:
		detailView.updateText(formulateDetailText())

func resetInfluenceSpriteVisibility():

	var influenceSprites = $AnimRoot/Influences.get_children()
	for influenceSprite in influenceSprites:
		influenceSprite.self_modulate = ColorN('white')


func getDecayModifier():

	if not slot:
		return 0

	return slot.decayModifier *-1

func dieToDecay():
	Events.emit_signal('TileDecayed')
	$RemovalAnimation.play('decayDie')
	yield($RemovalAnimation, 'animation_finished')
	removeSelf()

func removeSelf():

	if detailView:
		detailView.hide()

	if slot:
		slot.removeTile()
		Events.emit_signal('TileRemoved')		

	queue_free()

func setupFromObject(setupObject):

	if 'tileName' in setupObject:
		tileName = setupObject.tileName

	if 'emotionSprite' in setupObject:
		$AnimRoot/EmotionSprite.texture = load(setupObject.emotionSprite)

	influences = setupObject.influences
	if 'hp' in setupObject:
		$AnimRoot/Decay.setupHp(setupObject.hp)

	if 'multiplier' in setupObject:
		multiplier = setupObject.multiplier

	$AnimRoot/Decay.setupDecay(setupObject)

func scoreTile():

	Events.emit_signal('TileScored')
	$RemovalAnimation.play('score')

	yield($RemovalAnimation, 'animation_finished')
	removeSelf()

func formulateDetailText():
	return tileName

func _on_Shape_mouse_entered():
	print('mouse over tile')
	mouse_over = true

func _on_Shape_mouse_exited():
	print('mouse no longer over tile')
	mouse_over = false

func checkForDetailView():
	print('checking for Detail View')
	if mouse_over:
		Events.emit_signal('RequestDetailView', get_global_mouse_position(), formulateDetailText(), self)
		print('requesting detail view', self)
