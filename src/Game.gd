extends Node2D

onready var endGameScreen = get_node('Overlays/EndGameScreen')

func _ready():
	get_tree().paused = false
	Events.connect("GameOver", self, 'endGame')
	$TileManager.fillSlotsWithTiles()

func endGame():

	get_tree().paused = true
	endGameScreen.get_node('Label').text = endGameScreen.get_node('Label').text + str($Score.score)
	endGameScreen.popup_centered()

func _unhandled_input(event):

	if event.is_action_pressed("restart"):
		get_tree().reload_current_scene()

