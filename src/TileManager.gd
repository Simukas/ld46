extends Node2D

export (PackedScene) var packedTile
onready var emptySlots = get_children()
var tilesStack = []
var tileTakenCounter = 0

var exampleTileSpec = {
		'influences': [0,0,0,0,1,1],
		'tileName': 'DespairName',
		'decayAmount': 1,
		'decayIncrease': 0.5,
		'decayIncreaseTime': 5,
		'hp': 100,
		'multiplier': 5,
		'emotionSprite': 'res://art/Emotions/Despair.png'
	}

var tiles = [
	{
		'influences': [0,1,1,0,1,1],  #joy
		'emotionSprite': 'res://art/Emotions/Joy.png',
		'tileName': 'Joy'
	},
	{
		'influences': [1,1,1,1,1,1], #ecstasy
		'emotionSprite': 'res://art/Emotions/Extasy.png',
		'tileName': 'Ecstasy'
	},
	{
		'influences': [1,-1,0,0,-1,0], #despair
		'emotionSprite': 'res://art/Emotions/Despair.png',
		'tileName': 'Despair'
	},
	{
		'influences': [1,-1,1,0,-1,0], #prejudice
		'emotionSprite': 'res://art/Emotions/Prejudice.png',
		'tileName': 'Prejudice'
	},
	{
		'influences': [1,-1,0,-1,0,-1], #righteousness
		'emotionSprite': 'res://art/Emotions/Righteousness.png',
		'tileName': 'Righteousness'
	},
	{
		'influences': [0,-1,0,0,1,1], #intrigue
		'emotionSprite': 'res://art/Emotions/Intrigue1.png',
		'tileName': 'Intrigue'
	},
	{
		'influences': [0,-1,-1,0,0,1], #intrigue2
		'emotionSprite': 'res://art/Emotions/Intrigue2.png',
		'tileName': 'Intrigue'
	},
	{
		'influences': [0,-1,-1,0,1,1], #anger
		'emotionSprite': 'res://art/Emotions/Anger.png',
		'tileName': 'Anger'
	},
	{
		'influences': [0,1,0,-1,1,0], #melancholy
		'emotionSprite': 'res://art/Emotions/Melancholy.png',
		'tileName': 'Melancholy'
	},
	{
		'influences': [1,0,-1,-1,-1,0], #sadness
		'emotionSprite': 'res://art/Emotions/Sadness.png',
		'tileName': 'Sadness'
	},
	{
		'influences': [1,1,-1,-1,-1,1], #suprprise
		'emotionSprite': 'res://art/Emotions/Surprise1.png',
		'tileName': 'Surprise'
	},
	{
		'influences': [-1,-1,1,1,1,-1], #suprprise2
		'emotionSprite': 'res://art/Emotions/Surprise2.png',
		'tileName': 'Surprise'
	},
	{
		'influences': [-1,-1,0,-1,-1,0], #terror
		'emotionSprite': 'res://art/Emotions/Terror.png',
		'tileName': 'Terror'
	},
	{
		'influences': [-1,-1,-1,-1,-1,-1], #rage		 
		'emotionSprite': 'res://art/Emotions/Rage.png',
		'tileName': 'Rage'
	},
	{
		'influences': [0,-1,-1, 0,-1,-1],
		'emotionSprite': 'res://art/Emotions/Betrayal.png',
		'tileName': 'Betrayal' #betrayal
	},
	{
		'influences': [-1,0,-1, -1,-1,0],
		'emotionSprite': 'res://art/Emotions/Anxiety.png',
		'tileName': 'Anxiety' #anxiety
	}
]


func fillSlotsWithTiles():
	var cnt = 0
	while (cnt < get_children().size()):
		drawNewTile()
		cnt+=1


func _ready():
	randomize()
	tilesStack = tiles.duplicate()
	tilesStack.shuffle()
	var _c = Events.connect('FreeTileTaken', self, 'checkForTileRefresh')


#Auto Tile draw, not used
"""
func _process(delta):
	drawTileTimer += delta
	if drawTileTimer >= drawTileTime:
		drawTileTimer -= drawTileTime
		drawNewTile()
"""

#Reimplementing grid stuff 
func slotExists(_slotCoordinates):
	return true

func tileExists(_slotCoordinates):
	return true

func getSlot(_slotCoordinates):		
	return null

func updateAllSlotModifiers():
	pass

#End of reimplementing grid stuff

func drawNewTile():

	var emptySlot
	for slot in emptySlots:
		if not slot.tileContained:
			emptySlot = slot
			break

	if not emptySlot:
		#print('all slots filled')
		return

	if tilesStack.size() == 0:
		tilesStack = tiles.duplicate()
		tilesStack.shuffle()

	var tileSpec = tilesStack.pop_front()
	var newTile = packedTile.instance()
	newTile.setupFromObject(tileSpec)
	get_parent().add_child(newTile)
	emptySlot.addTile(newTile)
	newTile.recordSlot(emptySlot)

func _unhandled_input(event):

	if not Globals.debug:
		return

	if event.is_action_pressed("new_tile"):
		drawNewTile()
	
	if event.is_action_pressed("shuffle"):
		shuffleTileSpec()

	if event.is_action_pressed('discard1'):
		if emptySlots[0].tileContained:
			emptySlots[0].tileContained.removeSelf()

	if event.is_action_pressed('discard2'):
		if emptySlots[1].tileContained:
			emptySlots[1].tileContained.removeSelf()

	if event.is_action_pressed('discard3'):
		if emptySlots[2].tileContained:
			emptySlots[2].tileContained.removeSelf()

func shuffleTileSpec():
	tiles.shuffle()

func checkForTileRefresh():
	tileTakenCounter += 1

	if tileTakenCounter >= get_children().size():

		#need additional check because tiles can be rearrenged:
		var allEmpty = true
		for slot in emptySlots:
			if slot.tileContained:
				allEmpty = false
				break

		if allEmpty:
			Events.emit_signal('TileRefresh')
			fillSlotsWithTiles()
			tileTakenCounter = 0
