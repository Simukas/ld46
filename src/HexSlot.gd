extends Node2D

export var GridCoordinates = Vector2(0,0)
export var tileContained = false
export var tileDecayActive = true
export var containedTileCanBeMoved = false
export var freeTilePlace = false
var decayModifier = 0

onready var grid = get_parent()

class_name HexSlot

func _ready():
	$Label.text = str(GridCoordinates)
	if not Globals.debug:
		$Label.visible = false	

func addTile(tile):
	
	#if not tile is Tile:
	#	return

	if tileContained:
		return false

	tileContained = tile
	tile.position = position
	if not containedTileCanBeMoved:
		tile.movable = false

	grid.updateAllSlotModifiers()

	return true

func removeTile():

	if tileContained:
		tileContained = false

		if freeTilePlace:
			Events.emit_signal('FreeTileTaken')

		grid.updateAllSlotModifiers()

func highlight():
	$tile.visible = true
	$tile.self_modulate = ColorN('red')

func dehighlight():
	$tile.visible = false
	$tile.self_modulate = ColorN('white')

func getCoordinatesInDirection(directionNumber):
	#TODO: this is stupid but what can you do
	match directionNumber:
		0:
			return Vector2(GridCoordinates.x, GridCoordinates.y-1)
		3:
			return Vector2(GridCoordinates.x, GridCoordinates.y+1)
		1:
			if isEvenColumn():
				return Vector2(GridCoordinates.x+1, GridCoordinates.y)
			else:
				return Vector2(GridCoordinates.x+1, GridCoordinates.y-1)
		2:
			if isEvenColumn():
				return Vector2(GridCoordinates.x+1, GridCoordinates.y+1)
			else:
				return Vector2(GridCoordinates.x+1, GridCoordinates.y)
		4:
			if isEvenColumn():
				return Vector2(GridCoordinates.x-1, GridCoordinates.y+1)
			else:
				return Vector2(GridCoordinates.x-1, GridCoordinates.y)
		5:
			if isEvenColumn():
				return Vector2(GridCoordinates.x-1, GridCoordinates.y)
			else:
				return Vector2(GridCoordinates.x-1, GridCoordinates.y-1)
					

func isEvenColumn():
	return int(GridCoordinates.x) % 2 == 0

func getAvailableDirectionArray():
	var array = []
	var cnt = 0
	while (cnt<6):
		#print('chechking', getCoordinatesInDirection(cnt), 'grid says', grid.slotExists(getCoordinatesInDirection(cnt)))
		var coords = getCoordinatesInDirection(cnt)
		if grid.tileExists(coords):
			array.append('tile')
		elif grid.slotExists(coords):
			array.append('empty')
		else:
			array.append('wall')
		cnt+=1
	return array
