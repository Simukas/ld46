extends Node2D

export (NodePath) var person
export (float) var tileEffectMultiplier = 1.0
onready var slots = get_children()

func _ready():
	person = get_node(person)

func slotExists(slotCoordinates):
	return not not getSlot(slotCoordinates)

func tileExists(slotCoordinates):
	if slotExists(slotCoordinates):
		return getSlot(slotCoordinates).tileContained
	else:
		return false

func getSlot(slotCoordinates):
	for slot in slots:
		if slot.GridCoordinates == slotCoordinates:
			return slot
	
	return null

func updateAllSlotModifiers():

	resetAllDecayModifiers()

	for slot in slots:
		if slot.tileContained and slot.tileContained is Tile:
			var tile = slot.tileContained
			var dir = 0
			while (dir < 6):
				var inf = tile.influences[dir]
				var mult = tile.multiplier
				var coords = slot.getCoordinatesInDirection(dir)
				if slotExists(coords):
					getSlot(coords).decayModifier += inf * mult * tileEffectMultiplier
				dir+=1

	tryScoringStableGrid()

func resetAllDecayModifiers():

	for slot in slots:
		slot.decayModifier = 0

func checkGridStable():

	var tileWithNegative = false
	for slot in slots:
		if slot.tileContained and slot.tileContained is Tile:		
			if slot.decayModifier < slot.tileContained.decay.decayAmount:
				tileWithNegative = true
				break
	
	if tileWithNegative:
		print('Stability: tile with negative')
		return 'unstable'

	var personDecay = person.decay.decayAmount - person.getDecayModifier()*-1
	#print(person.decay.decayAmount,person.getDecayModifier()*-1)
	if personDecay > 0:
		print('Stability: person decay')
		return 'unstable'
	elif personDecay == 0:
		print('Stability: person stable')
		return 'stableNoHeal'
	elif personDecay < 0:
		print('Stability: person heal')
		return 'stableHeal'

func tryScoringStableGrid():

	var gridState = checkGridStable()
	match gridState:
		'stableHeal':
			person.restoreMaxHp()
			print('stabilityDetected')
			Events.emit_signal('Stability')
			for slot in slots:
				if slot.tileContained and slot.tileContained is Tile:
					slot.tileContained.scoreTile()
		'stableNoHeal':
			print('stabilityDetected')
			Events.emit_signal('Stability')
			for slot in slots:
				if slot.tileContained and slot.tileContained is Tile:
					slot.tileContained.scoreTile()
