extends Button

export (NodePath) var popupToOpen

func _ready():
	popupToOpen = get_node(popupToOpen)
	popupToOpen.connect('popup_hide', self, 'onPopupClose')

func _on_InfoButton_pressed():
	popupToOpen.popup_centered()
	get_tree().paused = true

func onPopupClose():
	get_tree().paused = false
