extends Label

export (int) var score = 0
export (int) var scoreIncrement = 1
export (int) var pointIncrementPerTileScored = 50
onready var bonus = get_node('AnimRoot/Bonus')
var scoreAnimations = 0 
var timer = 0

func _ready():
	Events.connect('TileScored', self, 'scoreTile')

func _process(delta):

	timer += delta
	if timer >= 1:
		score+= scoreIncrement
		timer -= 1
		text = 'Score: ' + str(score)

func scoreTile():

	if $AnimationPlayer.is_playing():
		scoreAnimations += 1
	
	else:
		bonus.text = '+' + str(pointIncrementPerTileScored)
		$AnimationPlayer.play('ShowBonus')

func _on_AnimationPlayer_animation_finished (_anim):

	Events.emit_signal('BonusScore')

	score += pointIncrementPerTileScored
	if scoreAnimations > 0:
		scoreAnimations-=1
		bonus.text = '+' + str(pointIncrementPerTileScored)
		$AnimationPlayer.play('ShowBonus')