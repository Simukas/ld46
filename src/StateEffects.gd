extends Node2D

export (Texture) var healSprite
export (Texture) var decaySprite

func heal():
	#print('heal effect')
	visible = true
	$StateSprite.texture = healSprite

func decay():
	#print('decay effect')
	visible = true
	$StateSprite.texture = decaySprite

func neutral():
	#print('neutral effect')
	visible = false
