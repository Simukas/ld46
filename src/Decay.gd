extends Node2D

signal Healed ()
signal Damaged()
signal NoChange()
signal Decayed ()

var decayed = false
var active = false
var decayingInstance

#default decay options

export (int) var startHp = 100
export (int) var decayAmount = 5
export (int) var decayIncrease = 3
export (int) var decayIncreaseTime = 5

var hp 
var decayTimer = 0 
var decayIncreaseTimer = 0

func _ready():
	hp = startHp
	$TextureProgress.value = (hp / float(startHp)) * 100

func _process(delta):
	#printt( delta, hp)

	if decayed or not active:
		return

	var tileModifier = decayingInstance.getDecayModifier()

	decayIncreaseTimer += delta
	if decayIncreaseTimer >= decayIncreaseTime:
		decayAmount += decayIncrease
		decayIncreaseTimer -= decayIncreaseTime

	decayTimer += delta
	if decayTimer >= 1:

		var oldHp = hp
		hp -= decayAmount + tileModifier
		decayTimer -= 1

	#	print(oldHp,hp)
		if oldHp < hp:
			emit_signal('Healed')
		elif oldHp > hp:
			emit_signal('Damaged')
		elif oldHp == hp:
			emit_signal('NoChange')

	if hp <= 0:
		decayed = true
		emit_signal('Decayed')
		$TextureProgress.value = 0
		return
	
	if hp >= startHp:
		hp = startHp

	$TextureProgress.value = (hp / float(startHp)) * 100

func setupHp(hpAmount):
	startHp = hpAmount
	hp = hpAmount

func setupDecay(decayObject):
	if 'decayAmount' in decayObject:
		decayAmount = decayObject.decayAmount

	if 'decayIncrease' in decayObject:
		decayIncrease = decayObject.decayIncrease

	if 'decayIncreaseTime' in decayObject:
		decayIncreaseTime = decayObject.decayIncreaseTime

func resetHp():
	hp = startHp
