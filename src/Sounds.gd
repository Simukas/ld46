extends Node

export (float) var soundEffectdb
export (float) var musicdb
export (float) var fadedMusicdb
onready var crossfadeTimer = get_node('CrossfadeTimer')
onready var crossfadeTime = crossfadeTimer.wait_time
onready var musicLength = get_node('Music').stream.get_length()

export (AudioStreamOGGVorbis) var takeSound1
export (AudioStreamOGGVorbis) var takeSound2
export (AudioStreamOGGVorbis) var takeSound3
export (AudioStreamOGGVorbis) var tileAppears
export (AudioStreamOGGVorbis) var deadTile
export (AudioStreamOGGVorbis) var the_end
export (AudioStreamOGGVorbis) var tilePlaced
export (AudioStreamOGGVorbis) var bonusScore
export (AudioStreamOGGVorbis) var stabilitySound

func playEffect(effectName):
	#print('play effect triggered', effectName)

	var streamPlayer = AudioStreamPlayer.new()
	add_child(streamPlayer)

	var soundEffect
	match effectName:
		'take':
			var randomNumber = randi() % 3
			if randomNumber == 0:
				soundEffect = takeSound1
			elif randomNumber == 1:
				soundEffect = takeSound2
			else:
				soundEffect = takeSound3
		'end':
			soundEffect = the_end
		'appears':
			soundEffect = tileAppears
		'dies':
			soundEffect = deadTile
		'placed':
			soundEffect = tilePlaced
		'bonus':
			soundEffect = bonusScore
		'stability':
			soundEffect = stabilitySound

	streamPlayer.stream = soundEffect
	streamPlayer.volume_db = soundEffectdb
	streamPlayer.play()
	yield(streamPlayer, 'finished')
	#print('play effect finished', effectName)	
	streamPlayer.queue_free()

func _ready():

	Events.connect('GameOver', self, 'playEffect', ['end'])
	Events.connect('TileDecayed', self, 'playEffect', ['dies'])
	Events.connect('TileDragStarted', self, 'playEffect', ['take'])
	Events.connect('TileRefresh', self, 'playEffect', ['appears'])
	Events.connect('TilePlaced', self, 'playEffect', ['placed'])
	Events.connect('BonusScore', self, 'playEffect', ['bonus'])
	Events.connect('Stability', self, 'playEffect', ['stability'])

	startStream1()

func startStream1():
	

	$TweenOut.interpolate_property($Music2, "volume_db", musicdb, fadedMusicdb, crossfadeTime)
	$TweenOut.start()
	$TweenIn.interpolate_property($Music, "volume_db", fadedMusicdb, musicdb, crossfadeTime)
	$TweenIn.start()
	yield(get_tree().create_timer(0.1), "timeout")
	$Music.play()
	var waitFor = musicLength - crossfadeTime
	crossfadeTimer.wait_time = waitFor
	crossfadeTimer.start()
	yield(crossfadeTimer, 'timeout')
	startStream2()

func startStream2():
	
	$TweenOut.interpolate_property($Music, "volume_db", musicdb, fadedMusicdb, crossfadeTime)
	$TweenOut.start()
	$TweenIn.interpolate_property($Music2, "volume_db", fadedMusicdb, musicdb, crossfadeTime)
	$TweenIn.start()
	yield(get_tree().create_timer(0.1), "timeout")
	$Music2.play()
	var waitFor = musicLength - crossfadeTime
	crossfadeTimer.wait_time = waitFor
	crossfadeTimer.start()
	yield(crossfadeTimer, 'timeout')
	startStream1()
