extends Node2D

export var PersonCoords = [Vector2(1,1), Vector2(1,2)]
export (NodePath) var grid
onready var decay = get_node('Decay')
onready var stateEffects = get_node('StateEffects')

func _ready():
	
	decay.active = true
	grid = get_node(grid)
	var _c = $Decay.connect('Decayed', self, 'die')
	decay.decayingInstance = self

	decay.connect('Healed', stateEffects, 'heal')
	decay.connect('Damaged', stateEffects, 'decay')
	decay.connect('NoChange', stateEffects, 'neutral')

func getDecayModifier():
	var modifier = 0
	for coords in PersonCoords:
		if grid.slotExists(coords):
			modifier += grid.getSlot(coords).decayModifier *-1

	return modifier

func die():
	Events.emit_signal('GameOver')
	print('game over')

func restoreMaxHp():
	decay.resetHp()
